extern crate math;
extern crate rand;
#[macro_use]
extern crate vst;

use rand::random;
use std::marker::PhantomData;
use std::sync::Arc;
use vst::api::{Events, Supported};
use vst::buffer::AudioBuffer;
use vst::event::Event;
use vst::plugin::{CanDo, Category, HostCallback, Info, Plugin, PluginParameters};
use vst::util::AtomicFloat;

// Bewtween versions, this should be "<next version>-dev":
const VERSION: &'static str = "0.0.18";

// DONE (0.0.6): Overdrive clipping
// DONE (0.0.7): Pameterize overdrive
// DONE (0.0.8): Pre-overdrive noise
// DONE (0.0.9): Crappy sample-based smoothing (will have different effect depending on bitrate)
// DONE (0.0.10): Polyphony - an arraay of voices!
// DONE (0.0.11): Non-zero attack/release time
// DONE (0.0.12): Multiple voices with detuning
// DONE (0.0.13): TODO: Detune left/right channels (maybe less at low frequencies)
// DONE (0.0.14, by switching phase from f32 to f64): Fix that things get very out of whack after sitting overnight
// DONE (0.0.15): Treat amplitude parameters as dB, convert accordingly, use NewType idiom (struct Decibels(f32)) to enforce conversion
// DONE (0.0.16): Envelope exponent parameters
// DONE (0.0.17): Make unison parameters
// DONE (0.0.18): Avoid aliasing by always using integer sample-length wavelengths
// DONE (0.0.18): Avoid oscillator discontinuities when changing pitch (e.g. when changing detune settings)
// TODO: Probably completely refactor this entire thing with more physicall-modelled
//   oscillators and components, each with their own output buffer
// TODO: Mono - allow more than one key to be pressed without messing things up!
// TODO: Alternate shapes
// TODO: start waveform at note_on

type SampleRate = f32;

trait Wrapper<X> {
    fn wrap(f: X) -> Self;
    fn unwrap(&self) -> X;
}

struct Flicks(i64);

impl Flicks {
    const PER_SECOND: i32 = 705600000;
    fn from_samples(samples: usize, rate: f64) -> Self {
        Self((samples as f64 / rate).round() as i64)
    }
    fn from_seconds(seconds: f64) -> Self {
        Self((seconds * Self::PER_SECOND as f64) as i64)
    }
}

struct Decibels {
    db_value: f32,
}
impl Decibels {
    const fn new(db_value: f32) -> Self {
        Self { db_value: db_value }
    }
    fn from_amplitude(a: f32) -> Self {
        Self::new(20f32 * a.log10())
    }
    fn to_amplitude(&self) -> f32 {
        10f32.powf(self.db_value / 20f32)
    }
}
impl Wrapper<f32> for Decibels {
    fn wrap(db_value: f32) -> Self {
        Self { db_value: db_value }
    }
    fn unwrap(&self) -> f32 {
        self.db_value
    }
}

struct Cents {
    cents: f32,
}
impl Cents {
    const fn new(cents: f32) -> Self {
        Self { cents: cents }
    }
    fn to_detune_factor(&self) -> f64 {
        2f64.powf(self.cents as f64 / 1200.0)
    }
}
impl Wrapper<f32> for Cents {
    fn wrap(cents: f32) -> Self {
        Self { cents: cents }
    }
    fn unwrap(&self) -> f32 {
        self.cents
    }
}

#[cfg(test)]
mod tests {
    use super::Decibels;

    #[test]
    fn test_db() {
        let a: f32 = 100f32; // Should be able to get exact answers for x where log10(x) is an integer
        let d = Decibels::from_amplitude(a);
        assert_eq!(a, d.to_amplitude());
    }
}

#[repr(u8)]
#[derive(Clone, Copy, PartialEq)]
enum ADSRState {
    ATTACK,
    DECAY,
    SUSTAIN,
    RELEASE,
    DEAD,
}

struct ADSR {
    initial_level: f32,
    attack_duration: f32,
    attack_exponent: f32,
    post_attack_level: f32,
    decay_duration: f32,
    decay_exponent: f32,
    sustain_level: f32,
    release_duration: f32,
    release_exponent: f32,
}

fn curve_value_at(v0: f32, v1: f32, dur: f32, exponent: f32, time: f32) -> f32 {
    let vdiff = v1 - v0;
    let trat = time / dur;
    //v0 + vdiff * trat
    if trat < 0f32 || trat > 1f32 {
        panic!(
            "trat = {}, outside the expected range of 0-1.  dur={}, time={}",
            trat, dur, time
        );
    }
    v0 + (vdiff * f32::powf(trat, exponent))
}

impl ADSR {
    /// Given an initial state and time offset since the beginning of the state,
    /// find and return the final state and offset from its beginning
    /// corresponding to that same point along the envelope
    fn get_updated_state_offset(&self, mut state: ADSRState, mut time: f32) -> (ADSRState, f32) {
        loop {
            match state {
                ADSRState::DEAD => {
                    return (ADSRState::DEAD, 0f32);
                }
                ADSRState::ATTACK => {
                    if time < self.attack_duration {
                        return (state, time);
                    }
                    state = ADSRState::DECAY;
                    time -= self.attack_duration;
                }
                ADSRState::DECAY => {
                    if time < self.decay_duration {
                        return (state, time);
                    }
                    state = ADSRState::SUSTAIN;
                    time -= self.attack_duration;
                }
                ADSRState::SUSTAIN => {
                    // Turned off by an event, not by time passing
                    return (state, time);
                }
                ADSRState::RELEASE => {
                    return if time < self.release_duration {
                        (state, time)
                    } else {
                        (ADSRState::DEAD, 0f32)
                    }
                }
            }
        }
    }

    fn get_value_within_state(&self, state: ADSRState, time: f32) -> f32 {
        match state {
            ADSRState::DEAD => {
                return 0f32;
            }
            ADSRState::ATTACK => {
                return curve_value_at(
                    self.initial_level,
                    self.post_attack_level,
                    self.attack_duration,
                    self.attack_exponent,
                    time,
                );
            }
            ADSRState::DECAY => {
                return curve_value_at(
                    self.post_attack_level,
                    self.sustain_level,
                    self.decay_duration,
                    self.decay_exponent,
                    time,
                );
            }
            ADSRState::SUSTAIN => {
                return self.sustain_level;
            }
            ADSRState::RELEASE => {
                return curve_value_at(
                    self.sustain_level,
                    0f32,
                    self.release_duration,
                    self.release_exponent,
                    time,
                );
            }
        }
    }

    /** Get the value of the ADSR curve at the given time after the start of the given state */
    fn get_value_at_state_offset(&self, state: ADSRState, time: f32) -> f32 {
        let (state, time) = self.get_updated_state_offset(state, time);
        self.get_value_within_state(state, time)
    }

    fn get_value_at(&self, time: f32) -> f32 {
        self.get_value_at_state_offset(ADSRState::ATTACK, time)
    }
}

type SampleIndex = usize;
type SampleDuration = i64;
type OscPosition = u32;
type OscWavelength = u32;

struct PhaseAccumulator {
    position_at: SampleIndex,
    position: OscPosition,
    wavelength: OscWavelength,
}

// When we're given a sample number,
// where within the sample do we want to sample?
const SAMPLE_SAMPLE_OFFSET: f32 = 0.5;

impl PhaseAccumulator {
    fn get_position_at(&self, time: SampleIndex) -> OscPosition {
        ((self.position as SampleDuration + (time - self.position_at) as SampleDuration)
            as SampleDuration
            % self.wavelength as SampleDuration) as OscPosition
    }
    fn get_phase_at(&self, time: SampleIndex) -> f32 {
        (self.get_position_at(time) as f32 + SAMPLE_SAMPLE_OFFSET) / self.wavelength as f32
    }
    /// Update the state of the oscillator before changing wavelength
    fn update_to(&mut self, time: SampleIndex) {
        self.position = self.get_position_at(time);
        self.position_at = time;
    }
    fn set_phase(&mut self, phase: f64, sample_rate: SampleRate) {
        self.position = (phase * sample_rate as f64).round() as OscPosition;
    }
    fn set_pitch(&mut self, pitch: f64, sample_rate: SampleRate) {
        self.wavelength = (sample_rate as f64 / pitch).round() as OscWavelength;
    }
}

//// Parameter management

struct AtomicX<X: Wrapper<f32>> {
    phantom: PhantomData<X>,
    atomic_float: AtomicFloat,
}
impl<X: Wrapper<f32>> AtomicX<X> {
    fn new(v: X) -> Self {
        Self {
            phantom: PhantomData,
            atomic_float: AtomicFloat::new(v.unwrap()),
        }
    }
    fn get(&self) -> X {
        X::wrap(self.atomic_float.get())
    }
    fn set(&self, v: X) {
        self.atomic_float.set(v.unwrap())
    }
}

type AtomicDecibels = AtomicX<Decibels>;
type AtomicCents = AtomicX<Cents>;

struct DecibelParameterMapping {
    min: Decibels,
    max: Decibels,
}
impl DecibelParameterMapping {
    fn encode(&self, value: Decibels) -> f32 {
        (value.db_value - self.min.db_value) / (self.max.db_value - self.min.db_value)
    }
    fn decode(&self, param_value: f32) -> Decibels {
        Decibels::new(param_value * (self.max.db_value - self.min.db_value) + self.min.db_value)
    }
}

struct LinearParameterMapping {
    min: f32,
    max: f32,
}
impl LinearParameterMapping {
    fn encode(&self, value: f32) -> f32 {
        (value - self.min) / (self.max - self.min)
    }
    fn decode(&self, param_value: f32) -> f32 {
        param_value * (self.max - self.min) + self.min
    }
}

struct CentsParameterMapping {
    min: Cents,
    max: Cents,
}
// TODO: Use Wrapper<f32> for these things
impl CentsParameterMapping {
    fn encode(&self, value: Cents) -> f32 {
        (value.cents - self.min.cents) / (self.max.cents - self.min.cents)
    }
    fn decode(&self, param_value: f32) -> Cents {
        Cents::new(param_value * (self.max.cents - self.min.cents) + self.min.cents)
    }
}

struct ExponentialParameterMapping {
    min_exponent: f32,
    max_exponent: f32,
}
impl ExponentialParameterMapping {
    fn encode(&self, value: f32) -> f32 {
        (value.log(2.0) - self.min_exponent) / (self.max_exponent - self.min_exponent)
    }
    fn decode(&self, param_value: f32) -> f32 {
        2f32.powf(param_value * (self.max_exponent - self.min_exponent) + self.min_exponent)
    }
}

struct QuadraticParameterMapping {
    exponent: f32,
    multiplier: f32,
}
impl QuadraticParameterMapping {
    fn encode(&self, value: f32) -> f32 {
        (value / self.multiplier).powf(1.0 / self.exponent)
    }
    fn decode(&self, param_value: f32) -> f32 {
        (param_value).powf(self.exponent) * self.multiplier
    }
}

const ENV_DURATION_PMAP: QuadraticParameterMapping = QuadraticParameterMapping {
    exponent: 4.0,
    multiplier: 10.0,
};
const ENV_EXPONENT_PMAP: ExponentialParameterMapping = ExponentialParameterMapping {
    min_exponent: -4.0,
    max_exponent: 4.0,
};
const SUSTAIN_PMAP: DecibelParameterMapping = DecibelParameterMapping {
    min: Decibels::new(-64.0),
    max: Decibels::new(6.0),
};
const OVERDRIVE_GAIN_PMAP: DecibelParameterMapping = DecibelParameterMapping {
    min: Decibels::new(0.0),
    max: Decibels::new(64.0),
};
const NOISE_LEVEL_PMAP: DecibelParameterMapping = DecibelParameterMapping {
    min: Decibels::new(-64.0),
    max: Decibels::new(0.0),
};
const UNISON_N_PMAP: LinearParameterMapping = LinearParameterMapping { min: 0.0, max: 8.0 };
const DETUNE_PMAP: CentsParameterMapping = CentsParameterMapping {
    min: Cents::new(0.0),
    max: Cents::new(100.0),
};
const UNISON_AMPLITUDE_FACTOR_PMAP: DecibelParameterMapping = DecibelParameterMapping {
    min: Decibels::new(-18.0),
    max: Decibels::new(18.0),
};

struct SynthParameters {
    noise_level: AtomicDecibels,
    overdrive_gain: AtomicDecibels,
    overdrive_wet: AtomicFloat,

    attack_duration: AtomicFloat,
    attack_exponent: AtomicFloat,
    decay_duration: AtomicFloat,
    decay_exponent: AtomicFloat,
    sustain_level: AtomicDecibels,
    release_duration: AtomicFloat,
    release_exponent: AtomicFloat,

    unison_n: AtomicFloat,
    unison_detune: AtomicCents,
    unison_amplitude_factor: AtomicDecibels,
    stereo_detune: AtomicCents,

    smoothing: AtomicFloat,
}

impl SynthParameters {
    fn get_amp_adsr(&self) -> ADSR {
        ADSR {
            initial_level: 0.0,
            attack_duration: self.attack_duration.get(),
            attack_exponent: self.attack_exponent.get(),
            post_attack_level: 1.0,
            decay_duration: self.decay_duration.get(),
            decay_exponent: self.decay_exponent.get(),
            sustain_level: self.sustain_level.get().to_amplitude(),
            release_duration: self.release_duration.get(),
            release_exponent: self.release_exponent.get(),
        }
    }
}

impl Default for SynthParameters {
    fn default() -> Self {
        Self {
            overdrive_gain: AtomicDecibels::new(Decibels::new(6.0)),
            overdrive_wet: AtomicFloat::new(1f32),
            noise_level: AtomicDecibels::new(Decibels::new(-20.0)),

            unison_n: AtomicFloat::new(2.0),
            unison_detune: AtomicCents::new(Cents::new(5.0)),
            unison_amplitude_factor: AtomicDecibels::new(Decibels::new(-3.0)),
            stereo_detune: AtomicCents::new(Cents::new(5.0)),

            attack_duration: AtomicFloat::new(0.001f32),
            attack_exponent: AtomicFloat::new(4.0),
            decay_duration: AtomicFloat::new(0.1f32),
            decay_exponent: AtomicFloat::new(1.0 / 4.0),
            sustain_level: AtomicDecibels::new(Decibels::new(-3.0)),
            release_duration: AtomicFloat::new(0.1f32),
            release_exponent: AtomicFloat::new(1.0 / 4.0),

            smoothing: AtomicFloat::new(0.75f32),
        }
    }
}

const OVERDRIVE_GAIN_IDX: i32 = 0;
const OVERDRIVE_WET_IDX: i32 = 1;
const NOISE_LEVEL_IDX: i32 = 2;
const SMOOTHING_IDX: i32 = 3;
const ATTACK_DURATION_IDX: i32 = 4;
const ATTACK_EXPONENT_IDX: i32 = 5;
const DECAY_DURATION_IDX: i32 = 6;
const DECAY_EXPONENT_IDX: i32 = 7;
const SUSTAIN_LEVEL_IDX: i32 = 8;
const RELEASE_DURATION_IDX: i32 = 9;
const RELEASE_EXPONENT_IDX: i32 = 10;
const UNISON_N_IDX: i32 = 11;
const UNISON_DETUNE_IDX: i32 = 12;
const UNISON_AMPLITUDE_FACTOR_IDX: i32 = 13;
const STEREO_DETUNE_IDX: i32 = 14;
const PARAMETER_COUNT: i32 = 15;

impl PluginParameters for SynthParameters {
    fn get_parameter_name(&self, index: i32) -> String {
        match index {
            OVERDRIVE_GAIN_IDX => "Overdrive Gain",
            OVERDRIVE_WET_IDX => "Overdrive Dry/Wet",
            NOISE_LEVEL_IDX => "Noise Level",
            SMOOTHING_IDX => "Smoothing",
            ATTACK_DURATION_IDX => "Attack Duration",
            ATTACK_EXPONENT_IDX => "Attack Exponent",
            DECAY_DURATION_IDX => "Decay Duration",
            DECAY_EXPONENT_IDX => "Decay Exponent",
            SUSTAIN_LEVEL_IDX => "Sustain Level",
            RELEASE_DURATION_IDX => "Release Duration",
            RELEASE_EXPONENT_IDX => "Release Exponent",
            UNISON_N_IDX => "Unison N",
            UNISON_DETUNE_IDX => "Unison Detune",
            UNISON_AMPLITUDE_FACTOR_IDX => "Unison Amp Factor",
            STEREO_DETUNE_IDX => "Stereo Detune",
            _ => "",
        }
        .to_string()
    }

    fn get_parameter_label(&self, index: i32) -> String {
        match index {
            OVERDRIVE_GAIN_IDX => "dB",
            OVERDRIVE_WET_IDX => "%",
            NOISE_LEVEL_IDX => "dB",
            SMOOTHING_IDX => "%",
            ATTACK_DURATION_IDX => "s",
            ATTACK_EXPONENT_IDX => "",
            DECAY_DURATION_IDX => "s",
            DECAY_EXPONENT_IDX => "",
            SUSTAIN_LEVEL_IDX => "dB",
            RELEASE_DURATION_IDX => "s",
            RELEASE_EXPONENT_IDX => "",
            UNISON_N_IDX => "",
            UNISON_DETUNE_IDX => "cents",
            UNISON_AMPLITUDE_FACTOR_IDX => "dB",
            STEREO_DETUNE_IDX => "cents",
            _ => "",
        }
        .to_string()
    }

    fn get_parameter_text(&self, index: i32) -> String {
        match index {
            OVERDRIVE_GAIN_IDX => format!("{:.1}", self.overdrive_gain.get().db_value),
            OVERDRIVE_WET_IDX => format!("{:.1}", self.overdrive_wet.get() * 100.0),
            NOISE_LEVEL_IDX => format!("{:.1}", self.noise_level.get().db_value),
            SMOOTHING_IDX => format!("{:.1}", self.smoothing.get() * 100.0),
            ATTACK_DURATION_IDX => format!("{:.4}", self.attack_duration.get()),
            ATTACK_EXPONENT_IDX => format!("{:.4}", self.attack_exponent.get()),
            DECAY_DURATION_IDX => format!("{:.4}", self.decay_duration.get()),
            DECAY_EXPONENT_IDX => format!("{:.4}", self.decay_exponent.get()),
            SUSTAIN_LEVEL_IDX => format!("{:.1}", self.sustain_level.get().db_value),
            RELEASE_DURATION_IDX => format!("{:.4}", self.release_duration.get()),
            RELEASE_EXPONENT_IDX => format!("{:.4}", self.release_exponent.get()),
            UNISON_N_IDX => format!("{:.1}", self.unison_n.get()),
            UNISON_DETUNE_IDX => format!("{:.1}", self.unison_detune.get().cents),
            UNISON_AMPLITUDE_FACTOR_IDX => {
                format!("{:.1}", self.unison_amplitude_factor.get().db_value)
            }
            STEREO_DETUNE_IDX => format!("{:.1}", self.stereo_detune.get().cents),
            _ => "".to_string(),
        }
    }

    fn get_parameter(&self, index: i32) -> f32 {
        match index {
            OVERDRIVE_GAIN_IDX => OVERDRIVE_GAIN_PMAP.encode(self.overdrive_gain.get()),
            OVERDRIVE_WET_IDX => self.overdrive_wet.get(),
            NOISE_LEVEL_IDX => NOISE_LEVEL_PMAP.encode(self.noise_level.get()),
            SMOOTHING_IDX => self.smoothing.get(),
            ATTACK_DURATION_IDX => ENV_DURATION_PMAP.encode(self.attack_duration.get()),
            ATTACK_EXPONENT_IDX => ENV_EXPONENT_PMAP.encode(self.attack_exponent.get()),
            DECAY_DURATION_IDX => ENV_DURATION_PMAP.encode(self.decay_duration.get()),
            DECAY_EXPONENT_IDX => ENV_EXPONENT_PMAP.encode(self.decay_exponent.get()),
            SUSTAIN_LEVEL_IDX => SUSTAIN_PMAP.encode(self.sustain_level.get()),
            RELEASE_DURATION_IDX => ENV_DURATION_PMAP.encode(self.release_duration.get()),
            RELEASE_EXPONENT_IDX => ENV_EXPONENT_PMAP.encode(self.release_exponent.get()),
            UNISON_N_IDX => UNISON_N_PMAP.encode(self.unison_n.get()),
            UNISON_DETUNE_IDX => DETUNE_PMAP.encode(self.unison_detune.get()),
            UNISON_AMPLITUDE_FACTOR_IDX => {
                UNISON_AMPLITUDE_FACTOR_PMAP.encode(self.unison_amplitude_factor.get())
            }
            STEREO_DETUNE_IDX => DETUNE_PMAP.encode(self.stereo_detune.get()),
            _ => 0.0,
        }
    }

    fn set_parameter(&self, index: i32, val: f32) {
        match index {
            OVERDRIVE_GAIN_IDX => self.overdrive_gain.set(OVERDRIVE_GAIN_PMAP.decode(val)),
            OVERDRIVE_WET_IDX => self.overdrive_wet.set(val),
            NOISE_LEVEL_IDX => self.noise_level.set(NOISE_LEVEL_PMAP.decode(val)),
            SMOOTHING_IDX => self.smoothing.set(val),
            ATTACK_DURATION_IDX => self.attack_duration.set(ENV_DURATION_PMAP.decode(val)),
            ATTACK_EXPONENT_IDX => self.attack_exponent.set(ENV_EXPONENT_PMAP.decode(val)),
            DECAY_DURATION_IDX => self.decay_duration.set(ENV_DURATION_PMAP.decode(val)),
            DECAY_EXPONENT_IDX => self.decay_exponent.set(ENV_EXPONENT_PMAP.decode(val)),
            SUSTAIN_LEVEL_IDX => self.sustain_level.set(SUSTAIN_PMAP.decode(val)),
            RELEASE_DURATION_IDX => self.release_duration.set(ENV_DURATION_PMAP.decode(val)),
            RELEASE_EXPONENT_IDX => self.release_exponent.set(ENV_EXPONENT_PMAP.decode(val)),
            UNISON_N_IDX => self.unison_n.set(UNISON_N_PMAP.decode(val)),
            UNISON_DETUNE_IDX => self.unison_detune.set(DETUNE_PMAP.decode(val)),
            UNISON_AMPLITUDE_FACTOR_IDX => self
                .unison_amplitude_factor
                .set(UNISON_AMPLITUDE_FACTOR_PMAP.decode(val)),
            STEREO_DETUNE_IDX => self.stereo_detune.set(DETUNE_PMAP.decode(val)),
            _ => (),
        }
    }
}

fn clamp(val: f32, min: f32, max: f32) -> f32 {
    if val < min {
        min
    } else if val > max {
        max
    } else {
        val
    }
}

//// Waveforms

type WavePhase = f32;

/// A Simple frequency-agnostic function with domain [0..1]
trait Waveform {
    fn get_value_at(&self, t: WavePhase) -> f32;
}

struct SnapeWaveform {
    overdrive_gain: f32,
    noise_level: f32,
    overdrive_wet: f32,
}
impl Waveform for SnapeWaveform {
    fn get_value_at(&self, t: WavePhase) -> f32 {
        let t_floor = t.floor();
        let t_subphase: f32 = (t - t_floor) as f32;
        let wave_val: f32 = t_subphase * 2f32 - 1f32;
        let noise_val: f32 = self.noise_level * random::<f32>();
        let pre_overdrive_val = wave_val * (1f32 - noise_val);
        let overdrive_val: f32 = clamp(pre_overdrive_val * self.overdrive_gain, -1f32, 1f32);
        pre_overdrive_val * (1f32 - self.overdrive_wet) + overdrive_val * self.overdrive_wet
    }
}

struct UnisonPart {
    phase_accumulator: PhaseAccumulator,
    amplitude: f32,
}
impl UnisonPart {
    fn get_value_at<W: Waveform>(&self, waveform: &W, sample: SampleIndex) -> f32 {
        self.amplitude * waveform.get_value_at(self.phase_accumulator.get_phase_at(sample))
    }
}
impl Default for UnisonPart {
    fn default() -> Self {
        Self {
            phase_accumulator: PhaseAccumulator {
                position: 0,
                position_at: 0,
                wavelength: 100,
            },
            amplitude: 1.0,
        }
    }
}

struct UnisonParameters {
    /// Number of copies of the waveform on each side
    n: u8,
    /// Relative frequency of next higher unison wave
    detune_factor: f64,
    stereo_detune_factor: f64,
    /// Amplitude of next wave
    amplitude_factor: f32,
}

struct Voice {
    unison_parts: Vec<UnisonPart>,

    /** The MIDI note number, used to associate on/off events */
    note: u8,
    state: ADSRState,
    /** Sample at which the state was last changed */
    state_sample_offset: SampleIndex,
    //velocity: f32,
    pitch: f64, // In Hz
    /** Accumulator, used for smoothing **/
    accum_r: f32,
    accum_l: f32,
}

impl Voice {
    fn update_state(&mut self, adsr: &ADSR, to_sample: SampleIndex, sample_rate: SampleRate) {
        let (new_state, new_state_time) = adsr.get_updated_state_offset(
            self.state,
            (to_sample - self.state_sample_offset) as f32 / sample_rate,
        );
        self.state = new_state;
        self.state_sample_offset = to_sample - (new_state_time * sample_rate) as SampleIndex;
    }

    fn update_oscillators(
        &mut self,
        unison_parameters: &UnisonParameters,
        to_sample: SampleIndex,
        sample_rate: SampleRate,
    ) {
        let osc_per_channel_count = 1 + unison_parameters.n * 2;
        let osc_count = osc_per_channel_count * 2;
        let left_detune_factor = unison_parameters.stereo_detune_factor;
        let right_detune_factor = 1.0 / left_detune_factor;
        let channel_detune_factors = [left_detune_factor, right_detune_factor];

        self.unison_parts
            .resize_with(osc_count as usize, Default::default);

        for osc in &mut self.unison_parts {
            // Avoid discontinuities when changing detunes?
            osc.phase_accumulator.update_to(to_sample);
        }

        let mut osc_num = 0;
        for channel_num in 0..2 {
            let mut amp = 1f32;
            let pitch = self.pitch * channel_detune_factors[channel_num];
            self.unison_parts[osc_num].amplitude = amp;
            self.unison_parts[osc_num]
                .phase_accumulator
                .set_pitch(pitch, sample_rate);
            osc_num += 1;

            let mut upper_pitch = pitch;
            let mut lower_pitch = pitch;
            for unison_num in 0..unison_parameters.n {
                upper_pitch *= unison_parameters.detune_factor;
                lower_pitch /= unison_parameters.detune_factor;
                amp *= unison_parameters.amplitude_factor;

                self.unison_parts[osc_num].amplitude = amp;
                self.unison_parts[osc_num]
                    .phase_accumulator
                    .set_pitch(upper_pitch, sample_rate);
                osc_num += 1;

                self.unison_parts[osc_num].amplitude = amp;
                self.unison_parts[osc_num]
                    .phase_accumulator
                    .set_pitch(lower_pitch, sample_rate);
                osc_num += 1;
            }
        }
    }

    fn get_value_at<W: Waveform>(
        &self,
        waveform: &W,
        amp_adsr: &ADSR,
        channel_num: u8,
        sample_num: SampleIndex,
        sample_rate: SampleRate,
    ) -> f32 {
        let amp_level = amp_adsr.get_value_at_state_offset(
            self.state,
            (sample_num - self.state_sample_offset) as f32 / sample_rate as f32,
        );

        let mut a: f32 = 0.0;
        let osc_count: usize = self.unison_parts.len() / 2;
        let osc_offset: usize = channel_num as usize * osc_count;
        for i in 0..osc_count {
            a += self.unison_parts[i + osc_offset].get_value_at(waveform, sample_num);
        }
        amp_level * a
    }
}

impl Default for Voice {
    fn default() -> Self {
        Self {
            unison_parts: vec![],
            note: 0,
            state: ADSRState::DEAD,
            state_sample_offset: 0,
            pitch: 400.0,
            accum_r: 0.0,
            accum_l: 0.0,
        }
    }
}

//// The Plugin

#[derive(Default)]
struct RustVSTDemo {
    sample_rate: SampleRate,
    sample_offset: SampleIndex,

    params: Arc<SynthParameters>,

    voices: Vec<Voice>,
}

impl Plugin for RustVSTDemo {
    fn get_info(&self) -> Info {
        Info {
            name: format!("Rust VST Demo v{version}", version = VERSION).to_string(),
            unique_id: 1690619057,
            midi_inputs: 1,
            inputs: 0,
            outputs: 2,
            parameters: PARAMETER_COUNT,
            category: Category::Synth,
            ..Default::default()
        }
    }

    fn new(_hcb: HostCallback) -> Self {
        return Self {
            ..Default::default()
        };
    }

    // It's good to tell our host what our plugin can do.
    // Some VST hosts might not send any midi events to our plugin
    // if we don't explicitly tell them that the plugin can handle them.
    fn can_do(&self, can_do: CanDo) -> Supported {
        match can_do {
            // Tell our host that the plugin supports receiving MIDI messages
            CanDo::ReceiveMidiEvent => Supported::Yes,
            // Maybe it also supports ather things
            _ => Supported::Maybe,
        }
    }

    fn set_sample_rate(&mut self, rate: f32) {
        self.sample_rate = rate;
    }

    fn get_parameter_object(&mut self) -> Arc<dyn PluginParameters> {
        Arc::clone(&self.params) as Arc<dyn PluginParameters>
    }

    fn process_events(&mut self, events: &Events) {
        let adsr = self.params.get_amp_adsr();
        for voice in &mut self.voices {
            voice.update_state(&adsr, self.sample_offset, self.sample_rate);
        }
        self.voices.retain(|voice: &Voice| match voice.state {
            ADSRState::DEAD => false,
            _ => true,
        });

        for event in events.events() {
            match event {
                Event::Midi(ev) => {
                    // See https://www.midi.org/specifications/item/table-1-summary-of-midi-message
                    match ev.data[0] {
                        // Note on
                        144 => {
                            let midfreq: f64 = 440.0;
                            let notenum = ev.data[1];
                            self.voices.push(Voice {
                                note: notenum,
                                state: ADSRState::ATTACK,
                                pitch: (midfreq / 32.0) * 2f64.powf((notenum - 9) as f64 / 12.0),
                                state_sample_offset: self.sample_offset,
                                ..Voice::default()
                            });
                        }
                        // Note off
                        128 => {
                            let notenum = ev.data[1];
                            for voice in &mut self.voices {
                                if voice.note == notenum && voice.state != ADSRState::RELEASE {
                                    voice.state = ADSRState::RELEASE;
                                    voice.state_sample_offset = self.sample_offset;
                                }
                            }
                        }
                        // Pitch is in data[1]
                        _ => (),
                    }
                }
                _ => (),
            }
        }
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        let (_, mut output_buffer) = buffer.split();

        for output_channel in output_buffer.into_iter() {
            for output_sample in output_channel {
                *output_sample = 0f32;
            }
        }

        let smoothing = self.params.smoothing.get();
        let accum_wet = f32::powf(smoothing, 1f32 / 4f32);
        let accum_dry = 1f32 - accum_wet;

        let snapewaveform: SnapeWaveform = SnapeWaveform {
            overdrive_gain: self.params.overdrive_gain.get().to_amplitude(),
            overdrive_wet: self.params.overdrive_wet.get(),
            noise_level: self.params.noise_level.get().to_amplitude(),
        };
        let unison_parameters: UnisonParameters = UnisonParameters {
            n: self.params.unison_n.get().floor() as u8,
            detune_factor: self.params.unison_detune.get().to_detune_factor(),
            amplitude_factor: self.params.unison_amplitude_factor.get().to_amplitude(),
            stereo_detune_factor: self.params.stereo_detune.get().to_detune_factor(),
        };

        let amp_adsr = self.params.get_amp_adsr();

        for voice in &mut self.voices {
            voice.update_oscillators(&unison_parameters, self.sample_offset, self.sample_rate);
            let mut channel_num: u8 = 0;
            for output_channel in output_buffer.into_iter() {
                let mut sample_num = self.sample_offset;
                for output_sample in output_channel {
                    // TODO: Avoid searching from the beginning for each sample

                    let new_value = voice.get_value_at(
                        &snapewaveform,
                        &amp_adsr,
                        channel_num,
                        sample_num,
                        self.sample_rate,
                    );

                    let accum: &mut f32 = match channel_num {
                        0 => &mut voice.accum_l,
                        _ => &mut voice.accum_r,
                    };

                    *accum = *accum * accum_wet + new_value * accum_dry;
                    *output_sample += *accum;
                    sample_num += 1;
                }
                channel_num += 1;
            }
        }
        self.sample_offset += buffer.samples();
    }
}

plugin_main!(RustVSTDemo);
